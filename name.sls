{% set lookup = {
    'ATM091FIL100': "ATM World",
    'SALTMASTER': "MASTER World",
    'PDVFIL': "PDV World",
} %}

{% set name = lookup[grains.id] %}

name: {{ name }}
